package com.example.quizapp

object Constants {
    const val USER_NAME : String = "USER_NAME"
    const val CORRECT_ANSWERS : String = "CORRECT_ANSWERS"
    const val TOTAL_ANSWERS : String = "TOTAL_ANSWERS"

    fun getQuestions() : ArrayList<Question> {
        var questions = ArrayList<Question>()
        var que1 = Question(
            0,
            "What country does this flag belong to?",
            R.drawable.flagczech,
            "Poland",
            "Denmark",
            "RPA",
            "Czech republic",
            4
        )
        questions.add(que1)
        que1 = Question(
            0,
            "What country does this flag belong to?",
            R.drawable.flagaustria,
            "France",
            "Germany",
            "Austria",
            "Ukraine",
            3
        )
        questions.add(que1)
        que1 = Question(
            0,
            "Who's that?",
            R.drawable.cat,
            "Lucjusz",
            "Kocjusz",
            "Kocjan",
            "Koteusz",
            1
        )
        questions.add(que1)
        return questions
    }
}