package com.example.quizapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.*

class FinalActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_final)
        val userName: String? = intent.getStringExtra(Constants.USER_NAME)
        val points: Int = intent.getIntExtra(Constants.CORRECT_ANSWERS, 0)
        val maxPoints: Int = intent.getIntExtra(Constants.TOTAL_ANSWERS, 0)
        val textName : TextView = findViewById(R.id.nameText)
        val textScore : TextView = findViewById(R.id.scoreText)
        val image : ImageView = findViewById(R.id.imageTrophy)
        val percentScore : Double = points.toDouble() / maxPoints.toDouble()
        textName.text = userName
        textScore.text = "Your score is $points out of $maxPoints"
        if (percentScore > 0.8) {
            image.setImageResource(R.drawable.throphy1)
        }
        else if (percentScore > 0.5) {
            image.setImageResource(R.drawable.throphy2)
        }
        else {
            image.setImageResource(R.drawable.throphy3)
        }
        val buttonStart : Button = findViewById(R.id.btnFinish)
        buttonStart.setOnClickListener {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
}