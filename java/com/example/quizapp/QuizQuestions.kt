package com.example.quizapp

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat
import java.lang.reflect.Type

class QuizQuestions : AppCompatActivity() , View.OnClickListener{
    private var userName : String? = null
    private var currentIndex : Int = 0
    private var selectedOption: Int = 0
    private var points: Int = 0
    private var progressBar : ProgressBar? = null
    private var tvProgress : TextView? = null
    private var ansOne: TextView? = null
    private var ansTwo: TextView? = null
    private var ansThree: TextView? = null
    private var ansFour: TextView? = null
    private var image: ImageView? = null
    private var quest: TextView? = null
    private var questionsList:  ArrayList<Question>? = null
    private var buttonSubmit: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz_questions)
        userName = intent.getStringExtra(Constants.USER_NAME)
        progressBar = findViewById(R.id.progressBar)
        tvProgress = findViewById(R.id.textProgress)
        ansOne = findViewById(R.id.textOption1)
        ansTwo = findViewById(R.id.textOption2)
        ansThree = findViewById(R.id.textOption3)
        ansFour = findViewById(R.id.textOption4)
        image = findViewById(R.id.imageQuestion)
        quest = findViewById(R.id.textQuestion)
        questionsList = Constants.getQuestions()
        progressBar?.max = questionsList?.size!!
        buttonSubmit = findViewById(R.id.btnSubmit)

        ansOne?.setOnClickListener(this)
        ansTwo?.setOnClickListener(this)
        ansThree?.setOnClickListener(this)
        ansFour?.setOnClickListener(this)
        buttonSubmit?.setOnClickListener(this)


        setQuestion()
    }

    private fun setQuestion(){
        setDefaultOptionView()
        selectedOption = 0
        val questNumber = currentIndex + 1
        val maxQuest = progressBar?.max
        progressBar?.progress = questNumber
        tvProgress?.text = "$questNumber / $maxQuest"
        ansOne?.text = questionsList!![currentIndex]?.optionOne
        ansTwo?.text = questionsList!![currentIndex]?.optionTwo
        ansThree?.text = questionsList!![currentIndex]?.optionThree
        ansFour?.text = questionsList!![currentIndex]?.optionFour
        image?.setImageResource(questionsList!![currentIndex]?.image)
        quest?.text = questionsList!![currentIndex]?.question
        buttonSubmit?.text = "SUBMIT"

    }

    private fun setDefaultOptionView(){
        selectedOption = 0
        var optionList = ArrayList<TextView>()
        ansOne?.let {
            optionList.add(0, it)
        }
        ansTwo?.let {
            optionList.add(1, it)
        }
        ansThree?.let {
            optionList.add(2, it)
        }
        ansFour?.let {
            optionList.add(3, it)
        }
        for (answer in optionList){
            answer.setTextColor(Color.parseColor("#7E7E7E"))
            answer.typeface = Typeface.DEFAULT
            answer.background = ContextCompat.getDrawable(
                this,
                R.drawable.default_bg_option
            )
        }

    }
    private fun selectedOptionView(tv: TextView, selectedOpt: Int){
        if(selectedOption == -1){
            return
        }
        setDefaultOptionView()
        selectedOption = selectedOpt
        tv.setTextColor(Color.parseColor("#363A43"))
        tv.setTypeface(tv.typeface, Typeface.BOLD)
        tv.background = ContextCompat.getDrawable(
            this,
            R.drawable.selected_bg_option
        )
    }
    override fun onClick(view: View?) {
        if (view != null) {
            when(view.id){
                R.id.textOption1 -> ansOne?.let {
                    selectedOptionView(it, 1)
                }
                R.id.textOption2 -> ansTwo?.let {
                    selectedOptionView(it, 2)
                }
                R.id.textOption3 -> ansThree?.let {
                    selectedOptionView(it, 3)
                }
                R.id.textOption4 -> ansFour?.let {
                    selectedOptionView(it, 4)
                }
                R.id.btnSubmit -> {
                    if(selectedOption == 0){
                        Toast.makeText(this,"Please select an option",Toast.LENGTH_SHORT).show()
                    }
                    else if(selectedOption == -1){
                        if(questionsList!!.size != currentIndex+1) {
                            currentIndex++
                            setQuestion()
                        }
                        else{
                            Log.i("EEE","EEE1")
                            val intent = Intent(this, FinalActivity::class.java)
                            intent.putExtra(Constants.USER_NAME, userName)
                            intent.putExtra(Constants.TOTAL_ANSWERS, questionsList?.size)
                            intent.putExtra(Constants.CORRECT_ANSWERS, points)
                            startActivity(intent)
                            finish()
                        }
                    }
                    else{
                        val question = questionsList?.get(currentIndex)
                        if (question!!.correctAnswer == selectedOption){
                            points ++
                            answerView(selectedOption, R.drawable.correct_bg_option)
                        }
                        else{
                            answerView(selectedOption,R.drawable.wrong_bg_option)
                            answerView(question!!.correctAnswer, R.drawable.correct_bg_option)

                        }
                        if (questionsList!!.size == currentIndex+1){
                            buttonSubmit?.text = "FINISH"
                        }
                        else{
                            buttonSubmit?.text = "NEXT QUESTION"
                        }
                        selectedOption = -1
                    }
                }
            }
        }
    }
    private fun answerView(answer:Int, drawableView:Int){
        when(answer){
            1->ansOne?.background =ContextCompat.getDrawable(
                this,
                drawableView
            )
            2->ansTwo?.background =ContextCompat.getDrawable(
                this,
                drawableView
            )
            3->ansThree?.background =ContextCompat.getDrawable(
                this,
                drawableView
            )
            4->ansFour?.background =ContextCompat.getDrawable(
                this,
                drawableView
            )
        }
    }
}