package com.example.quizapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val buttonStart : Button = findViewById(R.id.buttonStart)
        val name : EditText = findViewById(R.id.etName)
        buttonStart.setOnClickListener {
            if (name.text.isNotEmpty()){
                val intent = Intent(this, QuizQuestions::class.java)
                intent.putExtra(Constants.USER_NAME, name.text.toString())
                startActivity(intent)
                finish()
            }
            else{
                Toast.makeText(this, "Please enter your name first!", Toast.LENGTH_LONG).show()
            }
        }


    }
}